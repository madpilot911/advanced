const loadBtn = document.querySelector(".load-btn");
const container = document.querySelector(".container");

loadBtn.addEventListener("mouseover", (e) => {
    const sound = document.querySelector("#sound-hover");
    sound.play().catch((err) => console.log(err));
});
loadBtn.addEventListener("click", (e) => {
    const sound = document.querySelector("#sound");
    sound.play();

    e.target.remove();
    loadFilms();
});

const muteBtn = document.querySelector(".mute");
muteBtn.addEventListener("click", (e) => {
    const sounds = document.querySelectorAll("audio");
    sounds.forEach((sound) => (sound.muted = false));
    muteBtn.remove();
});

function loadFilms() {
    fetch("https://swapi.dev/api/films/")
        .then((response) => {
            if (response.status >= 200 && response.status <= 300) {
                return response.json();
            } else console.error(response.status);
        })
        .then((data) => data.results)
        .then((data) =>
            data.forEach((film) => {
                createFilmCard(
                    ({ episode_id, title, opening_crawl, characters } = film)
                );
            })
        )
        .catch((err) => {
            container.insertAdjacentHTML(
                "afterbegin",
                `<div class="error">Sorry something went wrong. Try again later please</div>`
            );
        });
}

function createFilmCard({ episode_id, title, opening_crawl, characters }) {
    const card = document.createElement("div");
    card.classList.add("card");
    card.insertAdjacentHTML(
        "afterbegin",
        `<div class = "id">${episode_id}</div>
    <div class = "title">${title}</div>
    <div class = "opening-container">
    <div class = "opening">${opening_crawl}</div>
    </div>
    `
    );

    createSpinner(card);
    Promise.all(
            characters.map((character) =>
                fetch(character).then((response) => {
                    if (response.status >= 200 && response.status <= 300) {
                        return response.json();
                    } else console.error(response.status);
                })
            )
        )
        .then((res) => createCharacterList(res, episode_id, card))
        .catch((err) => {
            deleteSpinner();
            card.insertAdjacentHTML("beforeend", "Loading failed...");
        });
    container.append(card);
    const soundMain = document.querySelector("#sound-main");
    soundMain.play();
}

function createSpinner(parent) {
    const spinner = document.createElement("div");
    spinner.classList.add("lds-dual-ring");
    parent.append(spinner);
}

function deleteSpinner() {
    const spinner = document.querySelector(".lds-dual-ring");
    spinner.remove();
}

function createCharacterList(characters, episode_id, parent) {
    const list = document.createElement("select");
    list.insertAdjacentHTML(
        "afterbegin",
        `<option>Characters overwiew</options>`
    );

    characters.forEach((character) => {
        const option = document.createElement("option");
        const { name, height, mass, hair_color, skin_color } = character;
        const characterInfo = document.createElement("div");
        characterInfo.insertAdjacentHTML(
            "afterbegin",
            `${name}:<br>Height - ${height} cm<br>Mass - ${mass} kg<br>Hair Color - ${hair_color}<br>Skin Color - ${skin_color}`
        );
        characterInfo.classList.add("characters-info");
        characterInfo.classList.add(name.split(" ").join("_") + episode_id);
        option.insertAdjacentHTML("afterbegin", name);
        list.append(option);
        parent.append(characterInfo);
    });
    list.addEventListener("change", (e) => {
        const clickedItem = document.querySelector(
            `.${e.target.value.split(" ").join("_") + episode_id}`
        );

        if (clickedItem) {
            clickedItem.classList.add("active");
        }
        e.target.value = "Characters overview";
        const overlay = document.createElement("div");
        overlay.classList.add("overlay");
        container.append(overlay);
        overlay.addEventListener("click", (e) => {
            const infoBlocks = document.querySelectorAll(".characters-info");
            infoBlocks.forEach((block) => block.classList.remove("active"));
            overlay.remove();
        });
    });
    deleteSpinner();
    parent.append(list);
}

function appendElement(parent, ...children) {
    children.forEach((child) => parent.append(child));
}

function insertHTML(elements, ...content) {
    for (let i = 0; i < elements.length; i++) {
        elements[i].insertAdjacentHTML("afterbegin", content[i]);
    }
}