const books = [{
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70,
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70,
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70,
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40,
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    },
];

const root = document.getElementById("root");
const list = document.createElement("ul");

function addClass(cssClass, ...elements) {
    elements.forEach((element) => element.classList.add(cssClass));
}

function createItemTemplate(data, parentNode) {
    try {
        const listItem = document.createElement("li");
        const author = document.createElement("span");
        const name = document.createElement("span");
        const price = document.createElement("span");
        addClass("list-item__author", author);
        addClass("list-item__name", name);
        addClass("list-item__price", price);
        author.insertAdjacentHTML("afterbegin", `${data.author}`);
        if (!data.author) {
            throw new Error("has no author");
        }
        name.insertAdjacentHTML("afterbegin", `${data.name}`);
        if (!data.name) {
            throw new Error("has no name");
        }
        price.insertAdjacentHTML("afterbegin", `${data.price} грн`);
        if (!data.price) {
            throw new Error("has no price");
        }

        listItem.append(author);
        listItem.append(name);
        listItem.append(price);
        parentNode.append(listItem);
    } catch (err) {
        console.error(`The book ${data.name} of ${data.author} ${err.message}`);
    }
}
books.forEach((book) => {
    createItemTemplate(book, list);
});

root.append(list);

addClass("list", list);
addClass("list-item", ...document.querySelectorAll("li"));