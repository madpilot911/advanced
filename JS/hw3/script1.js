function Employee(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
}
Employee.prototype = {
    get name() {
        return this._name;
    },
    /**
     * @param {string} name
     */
    set name(name) {
        this._name = name;
    },
    get age() {
        return this._age;
    },
    /**
     * @param {number} age
     */
    set age(age) {
        this._age = age;
    },
    get salary() {
        return this._salary;
    },
    /**
     * @param {number} salary
     */
    set salary(salary) {
        this._salary = salary;
    },
};

Employee.prototype.logSelf = function() {
    console.log(this);
    return this;
};

function Programmer(name, age, salary, lang) {
    Employee.call(this, name, age, salary);
    this._lang = lang;
}

Programmer.prototype = Object.create(Employee.prototype);
Object.defineProperty(Programmer.prototype, "salary", {
    get: function salary() {
        return this._salary * 3;
    },
});
const worker1 = new Employee("Megan", 27, 2300);
console.log(`get name --> ${worker1.name},
             set name --> ${(worker1.name = "Tanya")},
             get new name --> ${worker1.name}`);

const programmer = new Programmer("Grisha", 22, 1300, ["js", "go", "c#"]);
programmer.logSelf();
console.log(`get salary --> ${programmer.salary},
             set salary --> ${(programmer.salary = 1500)},
             get new salary --> ${programmer.salary}`);