{
    class Employee {
        constructor(name, age, salary) {
            this._name = name;
            this._age = age;
            this._salary = salary;
        }
        get name() {
                return this._name;
            }
            /**
             * @param {string} name
             */
        set name(name) {
            this._name = name;
        }
        get age() {
                return this._age;
            }
            /**
             * @param {number} age
             */
        set age(age) {
            this._age = age;
        }
        get salary() {
                return this._salary;
            }
            /**
             * @param {number} salary
             */
        set salary(salary) {
            this._salary = salary;
        }
        logSelf() {
            console.log(this);
            return this;
        }
    }

    class Programmer extends Employee {
        constructor(name, age, salary, lang) {
            super(name, age, salary);
            this._lang = lang;
        }

        get salary() {
            return this._salary * 3;
        }
    }

    const worker = new Employee("John", 39, 2000);
    worker.logSelf();

    const prog = new Programmer("Alfred", 33, 2200, ["js", "php"]);
    prog.logSelf();
    console.log(prog.salary);
}