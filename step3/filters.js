import { CardiologistVisit, DentistVisit, TheraupeutistVisit } from "./cards";
import { renderAll } from "./renderAllCards";

class Filter {
    constructor(...filters) {
        filters.forEach((filter) => (this[filter] = filter));
    }
    renderFilter(formRoot, cardRoot) {
        const filterInput = document.createElement("div");
        filterInput.insertAdjacentHTML(
            "afterbegin",
            `<select class="custom-select mr-sm-2" id="filter-form">
            <option value="default">Фильтровать / Показать все</option>`
        );
        formRoot.prepend(filterInput);
        for (let key in this) {
            document
                .querySelector("#filter-form")
                .insertAdjacentHTML(
                    "beforeend",
                    `<option value=${key}>${key}</option>`
                );
        }
        const select = document.querySelector("#filter-form");
        select.addEventListener("change", (e) => {
            if (document.querySelectorAll(".card")) {
                Array.from(document.querySelectorAll(".card")).forEach((card) =>
                    card.remove()
                );
            }
            fetch(`https://ajax.test-danit.com/api/v2/cards`, {
                    method: "GET",
                    headers: {
                        "Content-Type": "application/json",
                        Authorization: `Bearer ${localStorage.getItem("token")}`,
                    },
                })
                .then((response) => response.json())
                .then((response) => {
                    if (select.value !== "default") {
                        const result = response.filter(
                            (card) => card.urgency === select.value
                        );
                        if (!result.length) {
                            cardRoot.insertAdjacentHTML(
                                "beforebegin",
                                `<div class = "no-match">Извините, ничего не найдено</div>`
                            );
                        } else {
                            if (document.querySelector(".no-match")) {
                                document.querySelector(".no-match").remove();
                            }
                        }

                        result.forEach((card) => {
                            if (card.doctor === "Кардиолог") {
                                const filteredCard = new CardiologistVisit(card);
                                filteredCard.renderCard(cardRoot);
                            } else if (card.doctor === "Стоматолог") {
                                const filteredCard = new DentistVisit(card);
                                filteredCard.renderCard(cardRoot);
                            } else {
                                const filteredCard = new TheraupeutistVisit(card);
                                filteredCard.renderCard(cardRoot);
                            }
                        });
                    } else {
                        if (document.querySelector(".no-match")) {
                            document.querySelector(".no-match").remove();
                        }
                        renderAll();
                    }
                })
                .catch((err) => {
                    console.log(err);
                    root.insertAdjacentHTML(
                        "afterbegin",
                        `<div class="error">Что-то пошло ужасно не так.<br>Попробуйте больше не пользоваться нашими услугами`
                    );
                });
        });
    }
    renderSearch(formRoot, cardRoot) {
        const searchInput = document.createElement("div");
        searchInput.insertAdjacentHTML(
            "afterbegin",
            `<input class="form-control mr-sm-2" id="search-field" type="search" placeholder="Описание визита" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" id="search-btn" type="submit">Поиск</button>
            <button class="btn btn-outline-success my-2 my-sm-0" id="reset-btn" type="submit">Сбросить</button>`
        );
        formRoot.append(searchInput);
        const searchField = document.querySelector("#search-field");
        const searchBtn = document.querySelector("#search-btn");
        searchBtn.addEventListener("click", () => {
            if (document.querySelectorAll(".card")) {
                Array.from(document.querySelectorAll(".card")).forEach((card) =>
                    card.remove()
                );
            }
            if (searchField.value) {
                fetch(`https://ajax.test-danit.com/api/v2/cards`, {
                        method: "GET",
                        headers: {
                            "Content-Type": "application/json",
                            Authorization: `Bearer ${localStorage.getItem("token")}`,
                        },
                    })
                    .then((response) => response.json())
                    .then((response) => {
                        const result = response.filter(
                            (card) =>
                            card.description
                            .toLowerCase()
                            .split(" ")
                            .includes(searchField.value.toLowerCase()) ||
                            card.fullName
                            .toLowerCase()
                            .split(" ")
                            .includes(searchField.value.toLowerCase())
                        );

                        result.length > 0 ?
                            result.forEach((card) => {
                                if (card.doctor === "Кардиолог") {
                                    const filteredCard = new CardiologistVisit(card);
                                    filteredCard.renderCard(cardRoot);
                                } else if (card.doctor === "Стоматолог") {
                                    const filteredCard = new DentistVisit(card);
                                    filteredCard.renderCard(cardRoot);
                                } else {
                                    const filteredCard = new TheraupeutistVisit(card);
                                    filteredCard.renderCard(cardRoot);
                                }
                            }) :
                            cardRoot.insertAdjacentHTML(
                                "beforebegin",
                                `<div class = "no-match">Извините, ничего не найдено</div>`
                            );
                    })
                    .catch((err) => {
                        console.log(err);
                        root.insertAdjacentHTML(
                            "afterbegin",
                            `<div class="error">Что-то пошло ужасно не так.<br>Попробуйте больше не пользоваться нашими услугами`
                        );
                    });
            }
            document.querySelector("#reset-btn").addEventListener("click", () => {
                searchField.value = "";
                Array.from(document.querySelectorAll(".card")).forEach((card) =>
                    card.remove()
                );
                console.log("reseted");
                document.querySelector(".no-match").remove();
                renderAll();
            });
        });
    }
}

export { Filter };