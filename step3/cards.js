import { CardiologistForm, TheraupeutistForm, DentistForm } from "./form";

const doctors = ["Кардиолог", "Стоматолог", "Терапевт"];

class Card {
    constructor({ purpose, urgency, fullName, description }) {
        this.purpose = purpose;
        this.description = description;
        this.urgency = urgency;
        this.fullName = fullName;
        this.fetch = "https://ajax.test-danit.com/api/v2/cards";
    }

    getAdditionalInfo() {
        return `<p class = "card-text">Cрочность визита: <b>${this.urgency}</b></p>
        <p class = "card-text">Дополнительная информация: ${this.description}</p>`;
    }

    renderCard(root) {
        let html = `<div class="card bg-light mb-3" id = "card${this.id}" style="max-width: 18rem;">
      <div class="card-header">${this.doctor}
      <button type="button" id="close-${this.id}" class = "close-card" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button></div>
      <div class="card-body">
        <h5 class="card-title">${this.fullName}</h5>
        <div class = "btn-wrapper"><button type="button" class="btn btn-info show-more-${this.id}">Показать больше</button>
        <button type="button" class="btn btn-warning" id ="edit-btn${this.id}">Редактировать</button></div>    
      </div>`;
        const cardNode = document.createElement("div");
        cardNode.insertAdjacentHTML("afterbegin", html);
        root.append(cardNode);
        const extendBtn = document.querySelector(`.show-more-${this.id}`);
        extendBtn.addEventListener("click", () => {
            extendBtn.insertAdjacentHTML(
                "beforebegin",
                `<div id="add-info-${this.id}">${this.getAdditionalInfo()}
              <button type="button" class="btn btn-info show-less-${
                this.id
              }">Показать меньше</button></div>`
            );
            extendBtn.style.display = "none";
            const expandBtn = document.querySelector(`.show-less-${this.id}`);
            expandBtn.addEventListener("click", () => {
                document.querySelector(`#add-info-${this.id}`).remove();
                extendBtn.style.display = "block";
            });
        });
        const closeBtn = document.querySelector(`#close-${this.id}`);
        closeBtn.addEventListener("click", () => {
            fetch(`https://ajax.test-danit.com/api/v2/cards/${this.id}`, {
                method: "DELETE",
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`,
                },
            });
            this.clearCard();
        });
        if (document.querySelector(".no-cards")) {
            document.querySelector(".no-cards").remove();
        }
        const editBtn = document.querySelector(`#edit-btn${this.id}`);
        editBtn.addEventListener("click", () => {
            const form = this.form;
            console.log(form);
            form.renderForm(document.querySelector(".root"));
            const createBtn = document.querySelector(".create");
            const editBtn = document.createElement("button");
            document.querySelector("form").replaceChild(editBtn, createBtn);
            editBtn.classList.add("btn", "btn-warning");
            editBtn.innerHTML = "Save changes";
            editBtn.addEventListener("click", (e) => {
                e.preventDefault();
                const args = form.createVisitObject();
                args.doctor = this.doctor;
                this.editVisit(args, this.id);
            });
        });
    }
    clearCard() {
        document.querySelector(`#card${this.id}`).remove();
        if (!document.querySelector(".card")) {
            document
                .querySelector(".root")
                .insertAdjacentHTML(
                    "afterbegin",
                    `<div class="no-cards">No items have been added</div>`
                );
        }
    }
    setID(id) {
        this.id = id;
    }
    editVisit(body, id) {
        fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${localStorage.getItem("token")}`,
                },
                body: JSON.stringify(body),
            })
            .then((response) => response.json())
            .then((response) => {
                this.clearCard();
                const editedCard = new this.type(response);
                this.setID(response.id);
                editedCard.renderCard(document.querySelector(".root"));
                document.querySelector(".modal").remove();
                document.querySelector(".overlay").remove();
            })
            .catch((err) => {
                console.log(err);
                root.insertAdjacentHTML(
                    "afterbegin",
                    `<div class="error">Что-то пошло ужасно не так.<br>Попробуйте больше не пользоваться нашими услугами`
                );
            });
    }
}

class CardiologistVisit extends Card {
    constructor({
        purpose,
        urgency,
        fullName,
        bloodPressure,
        bmi,
        diseases,
        age,
        description,
    }) {
        super({ purpose, urgency, fullName, description });
        this.doctor = doctors[0];
        this.bloodPressure = bloodPressure;
        this.bmi = bmi;
        this.diseases = diseases;
        this.age = age;
        this.form = new CardiologistForm();
        this.type = CardiologistVisit;
    }

    postCard(root) {
        fetch(this.fetch, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${localStorage.getItem("token")}`,
                },
                body: JSON.stringify({
                    description: this.description,
                    doctor: this.doctor,
                    bloodPressure: this.bloodPressure,
                    age: this.age,
                    bmi: this.bmi,
                    purpose: this.purpose,
                    urgency: this.urgency,
                    fullName: this.fullName,
                    diseases: this.diseases,
                }),
            })
            .then((response) => response.json())
            .then((response) => {
                this.setID(response.id);
                return this;
            })
            .then((res) => res.renderCard(root))
            .catch((err) => {
                console.log(err);
                root.insertAdjacentHTML(
                    "afterbegin",
                    `<div class="error">Что-то пошло ужасно не так.<br>Попробуйте больше не пользоваться нашими услугами`
                );
            });
    }

    getAdditionalInfo() {
        return `${super.getAdditionalInfo()}<p class = "card-text">Возраст пациента: ${
      this.age
    }</p><p class = "card-text">Артериальное давление: ${
      this.bloodPressure
    }</p><p class = "card-text">Индекс массы тела: ${
      this.bmi
    }</p><p class = "card-text">Перенесенные заболевания: ${
      this.diseases ? this.diseases : "не было"
    }</p>`;
    }
}

class TheraupeutistVisit extends Card {
    constructor({ purpose, description, urgency, fullName, age }) {
        super({ purpose, description, urgency, fullName });
        this.age = age;
        this.doctor = doctors[2];
        this.form = new TheraupeutistForm();
        this.type = TheraupeutistVisit;
    }
    postCard(root) {
        fetch(this.fetch, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${localStorage.getItem("token")}`,
                },
                body: JSON.stringify({
                    description: this.description,
                    doctor: this.doctor,
                    purpose: this.purpose,
                    urgency: this.urgency,
                    fullName: this.fullName,
                    age: this.age,
                }),
            })
            .then((response) => response.json())
            .then((response) => {
                this.setID(response.id);
                return this;
            })
            .then((res) => res.renderCard(root))
            .catch((err) => {
                console.log(err);
                root.insertAdjacentHTML(
                    "afterbegin",
                    `<div class="error">Что-то пошло ужасно не так.<br>Попробуйте больше не пользоваться нашими услугами`
                );
            });
    }
    getAdditionalInfo() {
        return `${super.getAdditionalInfo()}<p class = "card-text">Возраст: ${
      this.age
    }</p>`;
    }
}

class DentistVisit extends Card {
    constructor({ purpose, description, urgency, fullName, lastVisit }) {
        super({ purpose, description, urgency, fullName });
        this.doctor = doctors[1];
        this.lastVisit = lastVisit;
        this.form = new DentistForm();
        this.type = DentistVisit;
    }

    postCard(root) {
        fetch(this.fetch, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${localStorage.getItem("token")}`,
                },
                body: JSON.stringify({
                    description: this.description,
                    doctor: this.doctor,
                    purpose: this.purpose,
                    urgency: this.urgency,
                    fullName: this.fullName,
                    lastVisit: this.lastVisit,
                }),
            })
            .then((response) => response.json())
            .then((response) => {
                this.setID(response.id);
                console.log(response);
                return this;
            })
            .then((res) => res.renderCard(root))
            .catch((err) => {
                console.log(err);
                root.insertAdjacentHTML(
                    "afterbegin",
                    `<div class="error">Что-то пошло ужасно не так.<br>Попробуйте больше не пользоваться нашими услугами`
                );
            });
    }

    getAdditionalInfo() {
        return `${super.getAdditionalInfo()}<p class = "card-text">Дата последнего визита: ${
      this.lastVisit
    }</p>`;
    }
}

export { doctors, Card, CardiologistVisit, TheraupeutistVisit, DentistVisit };