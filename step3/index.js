console.log("Hello there General Kenobi");
const header = document.querySelector(".header");
const root = document.querySelector(".root");
const filterContainer = document.querySelector(".filter");

import { renderAll } from "./renderAllCards";

import { createHeader } from "./header";
import { Filter } from "./filters";

renderAll();

const filter = new Filter("обычная", "приоритетная", "низкая");
filter.renderFilter(filterContainer, root);
filter.renderSearch(filterContainer, root);

createHeader(header);

export { root };