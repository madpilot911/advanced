import image from "./img/logo.png";
import { Modal } from "./modal";
import {
    Select,
    MainSelect,
    Input,
    TextArea,
    Form,
    CardiologistForm,
    TheraupeutistForm,
    DentistForm,
} from "./form";
import { doctors, arrayOfID, CardiologistVisit } from "./cards";

function createHeader(root) {
    let html = `<div id="header"> <a class="logo" href="#">
    <img src="${image}" width="60" height="60" alt="logo"> Lorem Ipsum Medical
  </a><button type="button" class="btn btn-primary" id="sign-btn">Войти</button></div>`;
    root.insertAdjacentHTML("afterbegin", html);
    const signBtn = document.querySelector("#sign-btn");
    const createBtn = document.createElement("div");
    const headerDiv = document.querySelector("#header");
    createBtn.innerHTML = `<button type="button" class="btn btn-primary" id="create-btn">Создать</button>`;
    if (!localStorage.getItem("token")) {
        signBtn.addEventListener("click", () => {
            const signForm = new Modal(`<form>
      <div class="form-group" id="sign-form">
      <label for="InputEmail">Email address</label>
      <input type="email" class="form-control" id="InputEmail" aria-describedby="emailHelp" placeholder="Enter email">
      <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
      </div>
      <div class="form-group">
      <label for="InputPassword">Password</label>
      <input type="password" class="form-control" id="InputPassword" placeholder="Password">
      </div>
      <div class="form-check">
      <input type="checkbox" class="form-check-input" id="Check">
      <label class="form-check-label" for="Check">Check me out</label>
      </div>
      <button type="button" class="btn btn-primary" id="submit-btn">Submit</button>
      </form>`);
            signForm.renderModal(document.querySelector(".root"));
            Array.from(document.querySelectorAll(".main")).forEach((btn) =>
                btn.remove()
            );

            document.querySelector("#submit-btn").addEventListener("click", () => {
                fetch("https://ajax.test-danit.com/api/v2/cards/login", {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json",
                        },
                        body: JSON.stringify({
                            email: document.querySelector("#InputEmail").value,
                            password: document.querySelector("#InputPassword").value,
                        }),
                    })
                    .then((response) => {
                        if (response.status >= 200 && response.status < 300) {
                            headerDiv.replaceChild(createBtn, signBtn);
                            return response.text();
                        } else {
                            const error = new Modal(
                                `<div>Неверные имя пользователя или пароль</div>`
                            );
                            error.renderModal(document.querySelector(".root"));
                        }
                    })
                    .then((token) => {
                        localStorage.setItem("token", token);
                    })
                    .catch((err) => {
                        console.log(err);
                        root.insertAdjacentHTML(
                            "afterbegin",
                            `<div class="error">Что-то пошло ужасно не так.<br>Попробуйте больше не пользоваться нашими услугами`
                        );
                    });
            });
        });
    } else {
        headerDiv.replaceChild(createBtn, signBtn);
    }
    createBtn.addEventListener("click", () => {
        const mainInput = new MainSelect(doctors);
        mainInput.renderMainSelect(
            document.querySelector(".root"),
            "Выберите врача"
        );
        mainInput.createForm(document.querySelector(".root"));
    });
}

export { createHeader };