import {
    doctors,
    CardiologistVisit,
    TheraupeutistVisit,
    DentistVisit,
} from "./cards";
import { Modal } from "./modal";

class Select {
    constructor(options) {
        options.forEach((option) => (this[option] = option));
    }
    renderSelect(label) {
        const options = [];
        let select = `<label for="form-select">${label}</label><select class="custom-select mr-sm-2 form-input" id="form-select">
          <option value="default">Выбрать</option>`;
        for (let key in this) {
            options.push(`<option value=${key}>${key}</option>`);
        }
        select += options.join(",") + "</select>";
        return select;
    }
}

class MainSelect extends Select {
    renderMainSelect(root, label) {
        const mainInput = new Modal(this.renderSelect(label));
        mainInput.renderModal(root);
    }
    deleteForm() {
        document.querySelector(".overlay").remove();
        document.querySelector(".modal").remove();
    }
    createForm(root) {
        document.querySelector(".create-btn").addEventListener("click", () => {
            if (document.querySelector("#form-select").value === doctors[0]) {
                const form = new CardiologistForm();
                this.deleteForm();
                form.renderForm(root);
            } else if (document.querySelector("#form-select").value === doctors[1]) {
                const form = new DentistForm();
                this.deleteForm();
                form.renderForm(root);
            } else if (document.querySelector("#form-select").value === doctors[2]) {
                const form = new TheraupeutistForm();
                this.deleteForm();
                form.renderForm(root);
            }
        });
    }
}

class Input {
    constructor(label) {
        this.label = label;
    }
    renderInput() {
        const input = ` <label for="formInput">${this.label}</label>
            <input type="text"  class="form-input" placeholder=${this.label}>`;
        return input;
    }
}

class TextArea {
    constructor(label) {
        this.label = label;
    }
    renderText() {
        const text = ` <label for="formText">${this.label}</label>
          <textarea class="form-input" placeholder=${this.label}></textarea>`;
        return text;
    }
}

class Form {
    constructor() {
        this.purpose = "Цель визита";
        this.description = "Описание визита";
        this.fullName = "Ф.И.О";
    }
    renderForm(root) {
        const form = `<form>
        <div class="form-group">
          ${new Input(this.purpose, "purpose").renderInput()}
        </div>
        <div class="form-group">
        ${new Select(["обычная", "приоритетная", "низкая"]).renderSelect(
          "Срочность визита"
        )}
        </div>
        <div class="form-group">
        ${new Input(this.fullName).renderInput()}
        </div>
        <div class="form-group last">
        ${new TextArea(this.description).renderText()}
        </div>
        <button type="button" class="btn btn-primary create">Create</button>
      </form>`;
        const modal = new Modal(form);
        modal.renderModal(root);
        document.querySelector(".create-btn").remove();
        document.querySelector(".btn-secondary").remove();
        this.createCard();
    }
}

class CardiologistForm extends Form {
    constructor() {
        super();
        this.bloodPressure = "Артериальное давление";
        this.bmi = "Индекс массы тела";
        this.diseases = "Перенесенные заболевания";
        this.age = "Возраст";
    }
    renderForm(root) {
        super.renderForm(root);
        const last = document.querySelector(".last");
        last.insertAdjacentHTML(
            "afterbegin",
            `<div class="form-group">
        ${new Input(this.bloodPressure).renderInput()}
        </div>
        <div class="form-group">
        ${new Input(this.bmi).renderInput()}
        </div>
        <div class="form-group">
        ${new Input(this.diseases).renderInput()}
        </div>
        <div class="form-group">
        ${new Input(this.age).renderInput()}
        </div>`
        );
    }
    createVisitObject() {
        const [
            purpose,
            urgency,
            fullName,
            bloodPressure,
            bmi,
            diseases,
            age,
            description,
        ] = Array.from(document.querySelectorAll(".form-input")).map(
            (input) => input.value
        );
        return {
            purpose,
            urgency,
            fullName,
            bloodPressure,
            bmi,
            diseases,
            age,
            description,
        };
    }
    createCard() {
        document.querySelector(".create").addEventListener("click", () => {
            const args = this.createVisitObject();
            const card = new CardiologistVisit(args);
            card.postCard(document.querySelector(".root"));
            document.querySelector(".modal").remove();
            document.querySelector(".overlay").remove();
        });
    }
}

class TheraupeutistForm extends Form {
    constructor(purpose, description, fullName, age) {
        super(purpose, description, fullName);
        this.age = age;
    }
    renderForm(root) {
        super.renderForm(root);
        const last = document.querySelector(".last");
        last.insertAdjacentHTML(
            "afterbegin",
            `<div class="form-group">
      ${new Input("Возраст").renderInput()}
      </div>`
        );
    }
    createVisitObject() {
        const [purpose, urgency, fullName, age, description] = Array.from(
            document.querySelectorAll(".form-input")
        ).map((input) => input.value);
        return {
            purpose,
            urgency,
            fullName,
            description,
            age,
        };
    }
    createCard() {
        document.querySelector(".create").addEventListener("click", () => {
            const args = this.createVisitObject();
            console.log(args);
            const card = new TheraupeutistVisit(args);
            card.postCard(document.querySelector(".root"));
            document.querySelector(".modal").remove();
            document.querySelector(".overlay").remove();
        });
    }
}

class DentistForm extends Form {
    constructor(purpose, description, fullName, lastVisitDate, age) {
        super(purpose, description, fullName);
        this.lastVisitDate = lastVisitDate;
        this.age = age;
    }
    renderForm(root) {
        super.renderForm(root);
        const last = document.querySelector(".last");
        last.insertAdjacentHTML(
            "afterbegin",
            `<div class="form-group">
      ${new Input("Дата послeднего визита").renderInput()}
      </div>
          `
        );
    }
    createVisitObject() {
        const [purpose, urgency, fullName, lastVisit, description] = Array.from(
            document.querySelectorAll(".form-input")
        ).map((input) => input.value);
        return {
            purpose,
            urgency,
            fullName,
            description,
            lastVisit,
        };
    }
    createCard() {
        document.querySelector(".create").addEventListener("click", () => {
            const args = this.createVisitObject();
            console.log(args);
            const card = new DentistVisit(args);
            card.postCard(document.querySelector(".root"));
            document.querySelector(".modal").remove();
            document.querySelector(".overlay").remove();
        });
    }
}

export {
    Select,
    MainSelect,
    Input,
    TextArea,
    Form,
    CardiologistForm,
    TheraupeutistForm,
    DentistForm,
};