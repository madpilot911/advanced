import { CardiologistVisit, TheraupeutistVisit, DentistVisit } from "./cards";
import { root } from "./index";

function renderAll() {
    fetch(`https://ajax.test-danit.com/api/v2/cards`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
        })
        .then((response) => response.json())
        .then((response) => {
            if (response.length) {
                response.forEach((res) => {
                    console.log(res);
                    if (res.doctor === "Кардиолог") {
                        const card = new CardiologistVisit(res);
                        card.id = res.id;
                        card.renderCard(root);
                    } else if (res.doctor === "Стоматолог") {
                        const card = new DentistVisit(res);
                        card.id = res.id;
                        card.renderCard(root);
                    } else {
                        const card = new TheraupeutistVisit(res);
                        card.id = res.id;
                        card.renderCard(root);
                    }
                });
            } else {
                root.insertAdjacentHTML(
                    "afterbegin",
                    `<div class="no-cards">No items have been added</div>`
                );
            }
        })
        .catch((err) => {
            console.log(err);
            root.insertAdjacentHTML(
                "afterbegin",
                `<div class="error">Что-то пошло ужасно не так.<br>Попробуйте больше не пользоваться нашими услугами`
            );
        });
}

export { renderAll };