class Modal {
    constructor(html) {
        this.html = html;
    }
    renderModal(root) {
        const modal = document.createElement("div");
        modal.classList.add("modal");
        modal.insertAdjacentHTML(
            "afterbegin",
            `<div class="close overlay"></div><div class="modal"  role="dialog">
                  <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              ${this.html}
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary main create-btn">Save changes</button>
              <button type="button" class="close btn btn-secondary main" data-dismiss="modal">Close</button>
            </div>
        </div>
      </div>`
        );
        root.append(modal);
        root.insertAdjacentHTML("afterbegin", `<div class="close overlay"></div>`);
        Array.from(document.querySelectorAll(".close")).forEach((close) =>
            close.addEventListener("click", () => {
                document.querySelector(".overlay").remove();
                document.querySelector(".modal").remove();
            })
        );
    }
}

export { Modal };